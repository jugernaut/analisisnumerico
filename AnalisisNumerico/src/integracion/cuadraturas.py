#! /usr/bin/python

__author__ = "mike"
__date__ = "$22/05/2019 04:53:40 PM$"

import numpy as np

def rectangulo(f, a, b, n):
    x = np.linspace(a, b, n)
    cuadratura = 0.0
    for i in range(len(x)-1):
        cuadratura += f(x[i])
    return ((b-a)/n)*cuadratura

def puntomedio(f, a, b, n):
    x = np.linspace(a, b, n)
    cuadratura = 0.0
    for i in range(len(x)-1):
        cuadratura += f((x[i]+x[i+1])/2)
    return ((b-a)/n)*cuadratura

def funcion(x):
    return x


if __name__ == "__main__":
    print puntomedio(funcion, 0.0, 5.0, 100)
