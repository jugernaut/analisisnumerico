#! /usr/bin/python

__author__ = "mike"
__date__ = "$12/04/2019 04:36:28 PM$"

import numpy as np

def factorizacionQR(mat):
    Q = np.empty_like(mat)
    R = np.zeros([mat.shape[1], mat.shape[1]])
    vi = np.zeros([mat.shape[1]])
    for i in range(mat.shape[1]):
        vi = mat[:,i]
        for j in range(i):
            R[j,i] = np.dot(Q[:,j].T, mat[:,i])
            vi -= np.dot(R[j,i],Q[:,j])
        R[i,i] = np.linalg.norm(vi, 2)
        Q[:,i] = vi/R[i,i]
    return Q, R
    
def qr(A):
    m, n = A.shape
    Q = np.eye(m)
    for i in range(n - (m == n)):
        H = np.eye(m)
        H[i:, i:] = make_householder(A[i:, i])
        Q = np.dot(Q, H)
        A = np.dot(H, A)
    return Q, A

if __name__ == "__main__":
    matA = np.array([[-1.0, -1.0, 1.0],[1.0, 3.0, 3.0],[-1.0, -1.0, 5.0],[1.0, 3.0, 7.0]])
    Q, R = factorizacionQR(matA)
    print Q
    print R
    print np.matmul(Q,R)

