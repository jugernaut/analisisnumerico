#! /usr/bin/python

import math
import numpy as np

def unonorma(x):
    norma = 0.0
    for i in range (len(x)):
        norma += abs(x[i])
    return norma

def dosnorma(x):
    norma = 0.0 
    for i in range (len(x)):
        norma += abs(x[i])**2
    return math.sqrt(norma)

def normasupremo(x):
    max=x[0]
    for i in range(1,len(x)):
        if x[i] > max:
            max = x[i]
    return max

def unonormaMat(mat):
    norma = 0.0
    max = 0.0
    for i in range(len(mat[0])):
        for j in range(len(mat)):
            max += abs(mat[j][i])
        if max > norma:
            norma = max
        max = 0.0    
    return norma  

def infnormaMat(mat):
    norma = 0.0
    max = 0.0
    for i in range(len(mat)):
        for j in range(len(mat[0])):
            max += abs(mat[i][j])
        if max > norma:
            norma = max
        max = 0.0    
    return norma
        
if __name__ == "__main__":
    x = [1,2,3,4]
    mat = [[1,2],[3,4]]
    
    #print unonorma(x)
    #print dosnorma(x)
    #print normasupremo(x)
    
    print unonormaMat(mat)
    print np.linalg.norm(mat, 1)
    
    print infnormaMat(mat)
    print np.linalg.norm(mat, np.inf)
    
