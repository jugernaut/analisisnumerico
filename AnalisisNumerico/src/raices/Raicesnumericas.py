#! /usr/bin/python
__author__ = "mike"
__date__ = "$1/03/2019 05:06:10 PM$"
import math

def f(x):
    #return x**3 + 5*x - 9
    #return (math.e**x+2**-x+2*math.cos(x)-6)
    return x**3 - 4*(x**2) + 20
    #return x**3 + math.sin(x**2) + x + 5
    
def df(x):
    return 3*x**2 + 5

def biseccion(f, N, Tol, a, b):
    fa, fb = f(a), f(b)
    n = 1
    if fa * fb > 0:
        raise Exception("No existe una raiz en este intervalo")
    while( N >= n ):
        x0 = ( a + b ) / 2.0
        fx = f(x0)
        if fa*fx > 0:
            a = x0
        else:
            b = x0
        x1 = ( a + b ) / 2.0
        if abs(f(x1)) <= Tol and abs((x0-x1)/x1) <= Tol:
            return "La raiz es: ", x1, "en la iteracion ", n
        print "La aproximacion es: ", x1, "en la iteracion ", n
        n+=1
    raise Exception("Se alcanzo el maximo numero de iteraciones y no se encontro raiz")

def secante(f, N, Tol, x0, x1):
    fx0, fx1 = f(x0), f(x1)
    n = 1
    while( N >= n ):
        xn = x1-fx1*((x1-x0)/float(fx1-fx0))
        if abs(f(xn)) <= Tol: #and abs((x0-x1)/x1) <= Tol:
            return "La raiz es: ", xn, "en la iteracion ", n
        print "La aproximacion es: ", xn, "en la iteracion ", n
        x0 = x1
        x1 = xn
        fx0 = f(x0)
        fx1 = f(x1)
        n+=1
    raise Exception("Se alcanzo el maximo numero de iteraciones y no se encontro raiz")
    return xn

def newton(f, df, N, Tol, x0):
    n = 1
    while( N >= n ):
        fx0 = f(x0)
        df0 = df(x0)
        xn = (x0 - (fx0/float(df0)))
        if abs(f(xn)) <= Tol: #and abs((x0-x1)/x1) <= Tol:
            return "La raiz es: ", xn, "en la iteracion ", n
        print "La aproximacion es: ", xn, "en la iteracion ", n
        x0 = xn
        n+=1
    raise Exception("Se alcanzo el maximo numero de iteraciones y no se encontro raiz")

if __name__ == '__main__':
    print 'Biseccion'
    print biseccion(f, 100, 0.0001, -5, 5)
    print 'Secante'
    print secante(f, 100, 0.0001, -5, 5)
    print 'Newton'
    print newton(f, df, 100, 0.0001, -5)
