#! /usr/bin/python

import numpy as np 
import matplotlib.pyplot as plt

__author__ = "mike"
__date__ = "$27/02/2019 04:14:44 PM$"

def funcion(x):
    #return (x**3) - 4*(x**2) + 20 #Tol = 0.01
    #return (x**3)+ np.sin(x**2) + (x) +5 #Tol = 0.00001
    #return (math.e**(x))+(2**(-x))+(2*np.cos(x))-6
    return (230*x**4)+(18*x**3)+(9*x**2)-(221*x)-9
    #return (x**2) +50

def grafica(f):
    # creamos un espacio de 1000 puntos igualmente espaciados -10 and 10
    x = np.linspace(-10, 10, 1000)
    
    #se generan los valores a garficar
    fx = f(x)
    f#x = (x**2) - 50
    
    #se genera la grafica
    #print fx
    plt.plot(x, fx)
    
    #mostramos la grafica
    plt.show()

def main():
    grafica(funcion)

if __name__ == '__main__':
    main()