#! /usr/bin/python

# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.

__author__ = "documentodigital"
__date__ = "$Aug 20, 2018 7:02:39 PM$"


'''
    Funcion para calcular de manera iterativa el valor de la serie de
    Fibonacci dado un numero.
'''
def fiboIterativo(n):
    f1=1
    f2=1
    for i in range(1,int(n)-1):
        f1,f2=f2,f1+f2
    return f2

'''
    Funcion para calcular de manera iterativa el factorial de un numero.
'''
def factIterativo(n):
    fact = 1
    for i in range(1,n + 1):
        fact = fact * i
    return fact

#def fiboRecursivo(n):

#def factRecursivo(n):

'''
    Funcion que muestra los movimientos minimos necesarios para resolver
    el problema de las torrer de Hanoi
'''        
def torreHanoi(n , pila_inicial, pila_auxiliar, pila_final):
    #Caso base cuando solo se tiene un solo disco
    if n == 1:
        print "Mover disco 1 desde la pila",pila_inicial,"a la pila",pila_final
        return
    #Primer llamado recursivo que consiste en el caso A)
    torreHanoi(n-1, pila_inicial, pila_final, pila_auxiliar)
    #Pasar el disco de la parte del fondo de la pila inicial a la pila final
    print "Mover disco",n,"de la pila",pila_inicial,"a la pila",pila_final
    #Segundo llamado recursivo que consiste en el caso B)
    torreHanoi(n-1, pila_auxiliar, pila_inicial, pila_final)


def main():
    '''print fiboIterativo(8)
    print factIterativo(5)
    #print fiboRecursivo(8)
    #print factRecursivo(5)'''
    torreHanoi(3, 'Inicial', 'Auxiliar', 'Objetivo')

if __name__ == "__main__":
    main()
