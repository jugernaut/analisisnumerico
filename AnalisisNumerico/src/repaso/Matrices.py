#! /usr/bin/python

__author__ = "mike"
__date__ = "$15/02/2019 04:38:30 PM$"
import numpy as np

'''Modulo para recordar como representar matrices en python'''

def dimension(mat):
    return (len(mat),len(mat[0]))

def esCuadrada(mat):
    return len(mat) == len(mat[0])

def traza(mat):
    tr = 0
    if esCuadrada(mat):
        for i in range(len(mat)):
            tr += mat[i][i]
        return tr
    else:
        print 'La traza solo esta definida para matrices cuadradas'
        
def imprimeMatriz(mat):
    for i in range(len(mat)):
        print mat[i]
        
def sumaMatriz(m1, m2):
    if len(m1[0]) == len(m2):
        #se crea la matriz resultante
        res = np.zeros((len(m1), len(m1[0])))
        #par de for's que sirven para recorrer ambas matrices
        for i in range(len(m1)):
            for j in range(len(m1[0])):
                #se asigna a res en la entrada adecuada el valor de la suma
                res[i][j] = m1[i][j] + m2[i][j]
        return res
    else:
        print 'No se pueden sumar estas 2 matrices'

#resta matrices
def restaMatriz(m1, m2):
    if len(m1[0]) == len(m2):
        #se crea la matriz resultante
        sub = np.zeros((len(m1), len(m1[0])))
        #par de for's que sirven para recorrer ambas matrices
        for i in range(len(m1)):
            for j in range(len(m1[0])):
                #se asigna a res en la entrada adecuada el valor de la suma
                res[i][j] = m1[i][j] - m2[i][j]
        return res
    else:
        print 'No se pueden restar estas 2 matrices'
        
def multiplicacionEscalar(m1, esc):
    for i in range(len(m1)):
        for j in range(len(m1[0])):
            #se asigna a res en la entrada adecuada el valor de multiplicacion
            m1[i][j] = m1[i][j]*esc
    return m1

#multiplicacion matrices
def multiplicacionMatrices(m1, m2):
    if len(m1[0]) == len(m2):
        res = [[0 for row in range(len(m1))] for col in range(len(m2[0]))]
        for i in range(len(m1)):
            for j in range(len(m2[0])):
                for k in range(len(m1[0])):
                    res[i][j] += m1[i][k] * m2[k][j]
        return res
    else:
        print 'No se pueden multiplicar estas 2 matrices'


# Determinante
# devoler el determinante de la matriz m1
# puedes asumir que m1 es cuadrada
def determinante(m1):
    if len(m1) == 2:
        return m1[0][0]*m1[1][1]-(m1[0][1]*m1[1][0])
    else:
        det = 0.0
        for n in range(len(m1[0])):
            det += ((-1)**n)*(m1[0][n])*determinante(subMatriz(n,m1))
        return det
    
#Funcion auxiliar para el calculo de la submatriz
def subMatriz(col, mat):
    sub = np.zeros((len(mat)-1, len(mat[0])-1))
    for i in range(1,len(mat)):
        for j in range(len(mat[0])):
            if j < col:
                sub[i-1][j] = mat[i][j]
            if j > col:
                sub[i-1][j-1] = mat[i][j]
    return sub

#potencia de una matriz
def potencia(mat, n):
    pot = multiplicacionMatrices(mat, mat)
    for i in range(1, n-1):
        pot = multiplicacionMatrices(pot, mat)
    return pot

def main():
    print 'Prueba de funciones basicas'
    #matriz de prueba
    a = [[1,2],[3,4]]
    #prueba de funciones basicas
    print dimension(a)
    print esCuadrada(a)
    print traza(a) 
    imprimeMatriz(a)
    
    #prueba suma matriz
    print 'Prueba suma matrices'
    b = sumaMatriz(a,a)
    imprimeMatriz(b)
    
    print 'Prueba multiplicacion escalar'
    #prueba multiplicacion escalar
    c = multiplicacionEscalar(a,2)
    imprimeMatriz(c)
    
    print 'Prueba producto de matrices'
    #prueba multiplicacion matrices
    c = multiplicacionMatrices(a,a)
    imprimeMatriz(c)
    
    print 'Comprobacion con numpy'
    #prueba con numpy
    m2 = np.matmul(a,a)
    print m2
    
    print 'Prueba potencia de matrices'
    #prueba potencia
    m3 = potencia(a,2)
    imprimeMatriz(m3)
    
    print 'Prueba determinante'
    #prueba determinante
    print determinante(m3)
    print np.linalg.det(m3)
    
    b = [[1,2],[3,4]]
    print(potencia(b,3))
    
    mat2 = np.array([[1,2],[3,4]])
    m2 = np.matmul(mat2,mat2)
    m2 = np.matmul(m2,mat2)
    print m2
    
if __name__ == "__main__":
    main()
    
