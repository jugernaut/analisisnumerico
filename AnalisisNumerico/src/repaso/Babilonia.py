#! /usr/bin/python

# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.

__author__ = "mike"
__date__ = "$29/01/2019 07:26:15 PM$"

def babilonia1(x):
    r0 = x
    r1 = 1
    e = 0.00001
    while r0-r1 >= e:
        r0 = (r0+r1)/2
        r1 = x/r0
    print 'la raiz cuadrada es', r0  

def babilonia2(x):
    e = 0.00001
    r0 = x
    if r0*r0 == x:
        print 'la raiz cuadrada es', r0  
    else:
        while True:
            r1 = ((x/r0)+r0)/2
            if r0-r1 > e:
                r0 = r1
            else:
                print 'la raiz cuadrada es', r1
                break   

def babiloniaR(x,n) :
    e = 1e-5
    r = (x + n / x)/2
    if abs(r**2 - n) < e :
        return r
    else :
        return babiloniaR(r,n) 

if __name__ == "__main__":
    babilonia1(25)
    babilonia2(25)
    print 'la raiz cuadrada es', babiloniaR(25/2,25)
