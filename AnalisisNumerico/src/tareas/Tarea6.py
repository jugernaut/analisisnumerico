#! /usr/bin/python
__author__ = "mike"
__date__ = "$1/03/2019 05:06:10 PM$"
import numpy as np

# algoritmo para descomposicion A=LU (HECHO!!!)
# n es el tamano de la dimension del problema
# la matriz A ya esta dada
def descomposicionLU(n, A, L, U):
    # inicialmente guardar la matriz A en la matriz U
    for i in range(0,n):
            for j in range(0,n):
                    U[i][j] = A[i][j]
    # eliminacion gaussiana
    for i in range(0,n):
            for j in range(i+1,n):
                    # guardar los factores de eliminacion gaussiana 
                    # en la matriz L
                    factor = U[j][i]/U[i][i]
                    L[j][i] = factor
                    # realizar eliminacion gaussiana en la matriz U
                    # para quedar de forma triangular superior
                    for k in range(i+1,n):
                            U[j][k] = U[j][k] - factor*U[i][k]
    return

# algoritmo para sustitucion hacia delante
# n es el tamano de la dimension del problema
# matriz L, vector b ya estan dados como parametros
# guardar los resultados en el vector y
# Ly=b
def sustDelante(n, L, b, y):
    return

# algoritmo para sustitucion hacia atras
# n es el tamano de la dimension del problema
# matriz U, vector y ya estan dados como parametros
# guardar los resultados en el vector x
# Ux=y
def sustAtras(n, U, y, x):
    return

# Inversa
# se debe devolver la matriz inversa de mat.
# puedes asumir que mat es una matriz de 3x3.
# puedes usar funciones de numpy EXCEPTO LA
# FUNCION QUE DEVUELVE LA INVERSA.
def inversa(mat):
    return mat


def main():
    # tamano del problema
    n = 2
    # construccion de la matriz A tamano (n,n)
    # matriz de coeficientes aleatorios multiplicada por un factor de 75
    A = 15*np.random.rand(n,n)
    # solucion x teorica (1,2,3,4,...,n)
    xTeor = 1+np.arange(n,dtype=np.float64).reshape(n,1)
    # vector b = A*xTeor, producto matriz A con vector xTeor 
    b = A.dot(xTeor)
    # asignar memoria para vector x (solucion aproximada/calculada)
    x = np.zeros( (n,1) )
    # asignar memoria para vector y
    y = np.zeros( (n,1) )
    # asignar memoria para matriz L (matriz triangular inferior)
    L = np.zeros( (n,n) )
    # asignar memoria para matriz U (matriz triangular superior)
    U = np.zeros( (n,n) )

    # llamar funcion de descomposicion LU para obtener/llenar matrices L,U
    descomposicionLU(n,A,L,U)

    print A

    print L

    print U	

    # llamar funcion sustDelante para obtener/llenar vector y
    sustDelante(n,L,b,y)

    # llamar funcion sustAtras para obtener/llenar vector x 
    # solucion aproximada/calculada
    sustAtras(n,U,y,x)

    # imprimir solucion aproximada/calculada
    # el vector impreso deberia ser parecido a (1, 2, 3, ..., n)
    print (x)

    #**********PRUEBA DE LA FUNCION INVERSA*************
    inv = inversa(A)
    print inv
    print np.linalg.inv(A)
	
main()
