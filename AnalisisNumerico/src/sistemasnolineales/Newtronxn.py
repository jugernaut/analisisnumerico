#! /usr/bin/python

import numpy as np
import math

def determinante(mat):
    return mat[0][0]*mat[1][1]-(mat[0][1]*mat[1][0])

def inversa(mat):
    det = determinante(mat)
    inversa = np.array(mat)
    inversa[0][0], inversa[1][1] = inversa[1][1], inversa[0][0]
    inversa[0][1], inversa[1][0] = -inversa[0][1], -inversa[1][0]
    inversa = (1/float(det))*inversa
    return inversa

# funcion f1 del punto extra de la tarea 4
def f1(x1, x2):
    return 2*x1-x2-math.e**(-x1)

# funcion f2 del punto extra de la tarea 4
def f2(x1, x2):
    return -x1+2*x2-math.e**(-x2)

# numerador de la matriz jacobiana (revisar presentacion en aveciencias)
def numerador(x1,x2):
    return 3+2*math.e**(-x2)+2*math.e**(-x1)+math.e**(-x1-x2)

# parcial (en este caso la parcial de f1 y f2 es la misma)
def par(x):
    return 2+math.e**(-x)

'''Metodo de Newton para sistemas
   de ecuaciones no lineales
   aprox: es la primera aproximacion
   en forma de vector'''
def JimyNewtron(aprox):
    # contador de iteraciones
    n = 0 
    while n < 100: # maximo numero de iteraciones 100
        '''es necesario calcular la matriz jacobiana
        y para ello se necesita una matriz vacia de 2x2'''
        jacobinv = np.zeros([2,2])
        
        # valores de la matriz jacobiana en todas sus entradas
        jacobinv[0][0] = par(aprox[1][0])/numerador(aprox[0][0], aprox[1][0])
        jacobinv[0][1] = 1/numerador(aprox[0][0], aprox[1][0])
        jacobinv[1][0] = 1/numerador(aprox[0][0], aprox[1][0])
        jacobinv[1][1] = par(aprox[0][0])/numerador(aprox[0][0], aprox[1][0])
    
        # guarda la evalucion de f1 y f2 en forma de vector
        fx = np.array(aprox)
        fx[0][0] = f1(aprox[0][0], aprox[1][0])
        fx[1][0] = f2(aprox[0][0], aprox[1][0])
        
        # FORMA ITERATIVA DEL METODO DE NEWTON PARA SISTEMAS NO LIENALES
        aprox = aprox - np.matmul(jacobinv, fx)
        
        # se incrementa el contador
        n+=1
    
    # El valor devuelto es la aproximacion
    return aprox    
        
if __name__ == '__main__':
    '''
    mat = np.array([[0,1],[2,3]])
    print mat
    inv = inversa(mat)
    print inv
    npinv = np.linalg.inv(mat)
    print npinv
    print np.matmul(mat, inv)'''
    
    #Pruebas Jimmy 
    ap = np.zeros([2,1])
    sol = JimyNewtron(ap)
    print 'Aproximacion de la solucion'
    print sol
    print '\nAproximacion evaluada en f1'
    print f1(sol[0][0], sol[1][0])

