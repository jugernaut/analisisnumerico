#! /usr/bin/python
__author__ = "mike"
__date__ = "$3/05/2019 03:56:54 PM$"
import numpy as np
import matplotlib.pyplot as plt


def poliLagrange(x):
    return 4*x**2-39*x+87

def poliNewton(x):
    return 52-47*(x-1)+14*(x-1)*(x-2)-6*(x-1)*(x-2)*(x-4)+2*(x-1)*(x-2)*(x-4)*(x-5)

def splineL1(x):
    return -0.5*x+1.5

def splineL2(x):
    return -0.125*x+0.75

def splineCua1(x):
    return -x+5.5

def splineCua2(x):
    return 0.64*x**2-6.76*x+18.46

def splineCua3(x):
    return -1.6*x**2+24.6*x-91.3

def splineCub1(x):
    return (-0.02259117)*((x-8)**3)+(1.6667*(11-x))+3.2033*(x-8)

def splineCub2(x):
    return (-0.01527)*(15-x)**3+(-0.10427)*(x-11)**3+2.494*(15-x)+2.78*(x-11)
    #return (-0.0169433)*(15-x)**3+(-0.00846)*(x-11)**3+2.3178*(15-x)+2.55337*(x-11)

def splineCub3(x):
    return

def splineCub4(x):
    return

def splineCub5(x):
    return

def grafica():
    # creamos un espacio de 1000 puntos igualmente espaciados -10 and 10
    x = np.linspace(0, 7, 100)
    
    #se generan los valores a garficar
    f1x = poliLagrange(x)
    f2x = poliNewton(x)
    
    #se genera la grafica
    #print fx
    plt.plot(x, f1x, label='Lagrange')
    plt.plot(x, f2x, label='Newton')
    plt.legend(loc='upper right')
    
    #mostramos la grafica
    plt.show()
    
def graficaSplineLineal():
    # creamos un espacio de 1000 puntos igualmente espaciados -10 and 10
    x1 = np.linspace(1, 2, 100)
    x2 = np.linspace(2, 4, 100)
    
    #se generan los valores a garficar
    f1x = splineL1(x1)
    f2x = splineL2(x2)
    
    #se genera la grafica
    #print fx
    plt.plot(x1, f1x)
    plt.plot(x2, f2x)
    
    #mostramos la grafica
    plt.show()
    
def graficaSplineCuadratico():
    # creamos un espacio de 1000 puntos igualmente espaciados -10 and 10
    x1 = np.linspace(3, 4.5, 100)
    x2 = np.linspace(4.5, 7, 100)
    x3 = np.linspace(7, 9, 100)
    
    #se generan los valores a garficar
    f1x = splineCua1(x1)
    f2x = splineCua2(x2)
    f3x = splineCua3(x3)
    
    #se genera la grafica
    #print fx
    plt.plot(x1, f1x)
    plt.plot(x2, f2x)
    plt.plot(x3, f3x)
    
    #mostramos la grafica
    plt.show()
    
def graficaSplineCubico():
    # creamos un espacio de 1000 puntos igualmente espaciados -10 and 10
    x1 = np.linspace(8, 11, 100)
    x2 = np.linspace(11, 15, 100)
    #x3 = np.linspace(7, 9, 100)
    
    #se generan los valores a garficar
    f1x = splineCub1(x1)
    f2x = splineCub2(x2)
    #f3x = splineCua3(x3)
    
    #se genera la grafica
    #print fx
    plt.plot(x1, f1x)
    plt.plot(x2, f2x)
    #plt.plot(x3, f3x)
    
    #mostramos la grafica
    plt.show()

if __name__ == "__main__":
    #grafica()
    #graficaSplineLineal()
    #graficaSplineCuadratico()
    print splineCub1(11)
    print splineCub2(12.7)
    graficaSplineCubico()
    
    '''
    mat = np.array([[14.0, 4.0, 0.0],[4.0,14.0,3.0],[0.0,3.0,14.0]])
    b = np.array([-6.5, -5.5, -5.5])
    
    x = np.linalg.solve(mat,b)
    print x'''
