import numpy as np

def DifDiv(x,f):
    '''
        x: conjunto de valores en el eje x
        f: conjunto de valores en el eje y
        F[0]: coeficientes del polinomio de Newton
    '''
    #generamos la matriz de diferencias divididas
    F = np.zeros((len(x),len(x)))
    #la primer columna siempre son los valores de y
    for i in range(len(x)):
        F[i][0] = f[i]
    #se calcula la columna j, tomado como base la columna previa
    for j in range(1,len(x)):
        for i in range(len(x)-j):
            #se deja fija la columna y se desplaza sobre los renglones
            F[i][j] = (F[i+1][j-1]-F[i][j-1])/(x[i+j]-x[i])
    return F[0]    

def interpoladorNewton(x, coef, xi):
    '''
        x: conjunto de valores en el eje x
        coef: coeficientes del polinomio de Newton
        xi: valor en el cual se busca interpolar
        resultado: valor de la interpolacion en el valor xi
    '''
    resultado = coef[0]
    for i in range(1, len(coef)):
        eval = coef[i]
        for j in range(i):
            eval *= (xi-x[j])
        resultado += eval
    return resultado

def interpoladorLagrange(f, xi):
    result = 0.0
    n = len(f)
    for i in range (n):
        term = f[i][1]
        for j in range(n):
            if (j!=i): 
                term = term*(xi - f[j][0])/(f[i][0] - f[j][0]); 
        result += term; 
    return result 

if __name__ == "__main__":
    x = (1,2,4,5,7)
    y = (52,5,-5,-40,10)
    coef = DifDiv(x,y)
    print coef
    print interpoladorNewton(x, coef, 3)
    
    f = ((1.0,52.0), (4.0, -5.0), (7.0, 10.0))
    print interpoladorLagrange(f, 3)
    