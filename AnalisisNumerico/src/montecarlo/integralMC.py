#! /usr/bin/python

__author__ = "mike"
__date__ = "$27/05/2019 04:00:43 PM$"

import numpy as np

def integralMonteCarlo(f, N):
    puntos = np.random.uniform(size=N)
    total=0
    for i in range(N):
        total+=f(puntos[i])
    aprox = total/N
    return aprox

def integralMonteCarloG(f, N, a, b):
    puntos = np.random.uniform(a, b, size=N)
    total=0
    for i in range(N):
        total+=f(puntos[i]*(b-a)+a)
    aprox = (b-a)*total/N
    return aprox

def integralMonteCarloTriple(f, N):
    x = np.random.uniform(size=N)
    y = np.random.uniform(size=N)
    z = np.random.uniform(size=N)
    total = 0
    for i in range(N):
        total+=f(x[i], y[i], z[i])
    aprox = total/N
    return aprox

def f(x):
    return x**2

def f3(x, y, z):
    return 2*x +y - z**2

def main():
    print integralMonteCarlo(f, 100000)
    
    print integralMonteCarloG(f, 10, 1.0, 2.0)
    
    print integralMonteCarloTriple(f3, 100000)

main()
