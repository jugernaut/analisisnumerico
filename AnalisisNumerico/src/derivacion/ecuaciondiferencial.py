#! /usr/bin/python
__author__ = "mike"
__date__ = "$13/05/2019 04:45:39 PM$"

import numpy as np

def diferenciasfinitas(a, b, delt, N):
    y = np.zeros([N])
    y[0] = b
    for i in range(N-1):
        y[i+1] = y[i]*(a*delt+1)
    return y


if __name__ == "__main__":
    y = diferenciasfinitas(0.5, 0.02, 0.01, 10)
    x = np.linspace(0, 9, 10)
    print x
    print y

    